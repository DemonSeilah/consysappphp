<?php
include 'config.php';

$tblFiltered = $_REQUEST['tbl_filtered'];
if ($tblFiltered == 'All') {
    $query = "";
} else if ($tblFiltered == 'F') {
    $query = "WHERE `status`='F'";
} else if ($tblFiltered == 'C') {
    $query = "WHERE `status`='C'";
} else if ($tblFiltered == 'A') {
    $query = "WHERE `status`='A'";
}

$response_array['array_data'] = array();
$result = $mysqli_connect->query("SELECT * FROM tbl_po_header $query ORDER BY po_number ASC");
while ($row = $result->fetch_array()) {
    if ($row['payment_type'] == "C") {
        $payment_type = "CASH";
    } else {
        $payment_type = "CHECK";
    }
    $response['po_id'] = $row['po_header_id'];
    $response['project_name'] = $row['project_name'];
    $response['requested_by'] = $row['requested_by'];
    $response['po_number'] = $row['po_number'];
    $response['po_reference'] = $row['po_reference'];
    $response['date'] = date('F j, Y', strtotime($row['date']));
    $response['po_status'] = $row['status'];
    $response['payment_type'] = $payment_type;
    $response['supplier'] = getSupplier($row['supplier_id']);
    $response['terms'] = array($row['terms']);
    $response['remarks'] = $row['remarks'];

    array_push($response_array['array_data'], $response);
}

echo json_encode($response_array);
