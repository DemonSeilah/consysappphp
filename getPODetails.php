<?php
include 'config.php';

$po_id = $_REQUEST['po_id'];

$totalsum = 0;
$response_array['array_data'] = array();
$result = $mysqli_connect->query("SELECT * FROM `tbl_po_detail` WHERE po_header_id='$po_id' AND `status`='0'");

$ctr = mysqli_num_rows($result);
while ($row = $result->fetch_array()) {
    $response['po_detail_id'] = $row['po_detail_id'];
    $response['po_header_id'] = $row['po_header_id'];
    $response['item'] = $row['item'];
    $response['description'] = $row['description'];
    $response['quantity'] = number_format($row['quantity'], 0);
    $response['price'] = number_format($row['per_unit'], 2);
    $response['brand'] = $row['brand'];
    $response['model'] = $row['model'];
    $response['encoded_by'] = $row['encoded_by'];
    $response['totalsum'] = getTotal($po_id);
    if ($ctr > 0) {
        $response['ctr'] = $ctr;
    } else {
        $response['ctr'] = 0;
    }
    array_push($response_array['array_data'], $response);
}


echo json_encode($response_array);
